from datetime import datetime, timedelta
from airflow import DAG
from airflow.operators.python_operator import PythonOperator

default_args = {
    'owner': 'you',
    'depends_on_past': False,
    'start_date': datetime(2023, 10, 8),
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 1,
    'retry_delay': timedelta(minutes=5),
}

dag = DAG(
    'generate_purchase_report',
    default_args=default_args,
    description='A simple DAG to generate a purchase report',
    schedule_interval=timedelta(days=1),
)

def generate_purchase_report():
    # Your logic to generate a purchase report
    print("Generating purchase report...")

generate_report_task = PythonOperator(
    task_id='generate_purchase_report',
    python_callable=generate_purchase_report,
    dag=dag,
)
