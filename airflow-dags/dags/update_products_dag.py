from datetime import datetime, timedelta
from airflow import DAG
from airflow.operators.python_operator import PythonOperator

default_args = {
    'owner': 'you',
    'depends_on_past': False,
    'start_date': datetime(2023, 10, 8),
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 1,
    'retry_delay': timedelta(minutes=5),
}

dag = DAG(
    'update_products',
    default_args=default_args,
    description='A simple DAG to update products',
    schedule_interval=timedelta(days=1),
)

def update_products():
    # Your logic to update products
    print("Updating products...")

update_products_task = PythonOperator(
    task_id='update_products',
    python_callable=update_products,
    dag=dag,
)
